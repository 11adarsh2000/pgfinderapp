package com.PGApp.Model;


public class PG {
        private String name;
        private Integer Id;
        private String owner;
        private double rent;
        private String category;
        
		public PG() {
			super();
			// TODO Auto-generated constructor stub
		}
		public PG(String name, Integer id, String owner, double rent, String category) {
			super();
			this.name = name;
			Id = id;
			this.owner = owner;
			this.rent = rent;
			this.category = category;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getId() {
			return Id;
		}
		public void setId(Integer id) {
			Id = id;
		}
		public String getOwner() {
			return owner;
		}
		public void setOwner(String owner) {
			this.owner = owner;
		}
		public double getRent() {
			return rent;
		}
		public void setRent(double rent) {
			this.rent = rent;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		@Override
		public String toString() {
			return "PG [name=" + name + ", Id=" + Id + ", owner=" + owner + ", rent=" + rent + ", category=" + category
					+ "]";
		}
				
		
}
