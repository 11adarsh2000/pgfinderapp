package com.PGApp.exception;

public class PGNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PGNotFoundException() {
		super();
	}

	public PGNotFoundException(String message) {
		super(message);
        System.out.println(message);
	}

}
