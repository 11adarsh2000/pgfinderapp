package com.PGApp.main;

import com.PGApp.Model.PG;
import com.PGApp.Service.PGService;
import com.PGApp.Service.PGServiceImpl;
import com.PGApp.exception.PGIdNotFoundExcepton;
import com.PGApp.exception.PGNotFoundException;
import com.PGApp.repository.DbManager;

import java.util.Arrays;
import java.util.List;

public class Client {
      public static void main(String[] args) throws Exception{
    	DbManager.openConnection();

        PGService client=new PGServiceImpl();
        client.addManyPg(Arrays.asList(
				new PG("Venkateshwara Pg",1,"Morris",7500.67,"Male"),
				new PG("Dhanvi Pg",2,"Kanetkar",7200,"Male"),
				new PG("Rishi Pg",3,"Hellen",7600,"Male"),
				new PG("Raghu Pg",4,"Raghu",8000,"Both"),
				new PG("Mohan Pg",5,"Berley",8200,"Female"),
				new PG("Your Pg",6,"Karris",8500,"Female"),
				new PG("Manohar Pg",7,"Morris",8700,"Male"),
				new PG("Stanza Living",8,"Morris",9000,"Female"),
				new PG("Zolo Pg",9,"Sorris",6000,"Male"),
				new PG("Kavya Pg",10,"Raghu",6500,"Female"),
				new PG("Rental Pg",11,"Aman",9500,"Both"),
				new PG("Lohiya Pg",12,"Anna",12000,"Female"),
				new PG("Lodhi Pg",13,"Govind",10000,"Both"),
				new PG("Monika Pg",14,"Laxman",11000,"Male"),
				new PG("Kamohar Pg",15,"Govind",10500,"Male"),
				new PG("Stanza Living 2",16,"Patel",11200,"Female")
				));
		try {
			
           client.deletePg("Hell");
           System.out.println("Deleted");
		}catch(PGNotFoundException e) {
			e.getMessage();
		}
		try {
		client.updatePg("Morr", 9800);
		System.out.println("Updated");
		}catch(PGNotFoundException e) {
			e.getMessage();
		}
		
			System.out.println("All the Pgs");
			List<PG> listOfAll=client.getAll();
			for(PG b:listOfAll) {
				System.out.println(b);
			}
			
		try {	
			System.out.println("All the Pgs with Owner");
			List<PG> listOfPgByOwner=client.getByOwner("Govind");
			for(PG b:listOfPgByOwner) {
				System.out.println(b);
			}
		}catch(PGNotFoundException e) {
			e.getMessage();
		}
		try {	
			System.out.println("All the Pgs with Category");
			List<PG> listOfPgByCategory=client.getByCategory("Male");
			for(PG b:listOfPgByCategory) {
				System.out.println(b);
			}
		}catch(PGNotFoundException e) {
			e.getMessage();
		}
		try {
		    System.out.println("All the Pgs with Rent");
			List<PG> listOfPgByLesserRent=client.getByLesserRent(10);
			for(PG b:listOfPgByLesserRent) {
				System.out.println(b);
			}
		}catch(PGNotFoundException e) {
			e.getMessage();
		}
		try {
			System.out.println("All the Pgs with Id");
			PG PgById=client.getById(2);
				System.out.println(PgById);
			}catch(PGIdNotFoundExcepton e) {
			e.getMessage();
		}
	}
}
