package com.PGApp.repository;
import com.PGApp.Model.PG;
import com.PGApp.exception.PGIdNotFoundExcepton;
import com.PGApp.exception.PGNotFoundException;

import java.util.*;
public interface PGRepo {
	void addPg(PG pG);
    void updatePg(String owner,double rent) throws  PGNotFoundException;
    void deletePg(String owner) throws  PGNotFoundException;   
	
       List<PG> findAll();
       List<PG> findByOwner(String owner) throws  PGNotFoundException;
       List<PG> findByCategory(String category) throws PGNotFoundException;
       PG findById(int PgId) throws PGIdNotFoundExcepton;
       List<PG> findByLesserRent(double rent) throws PGNotFoundException;
	    void addManyPg(List<PG> pG);
}
