package com.PGApp.repository;

import java.util.*;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.BookApp.Model.Book;
import com.BookApp.exception.bookNotFoundException;
import com.BookApp.repository.DbManager;
import com.PGApp.Model.PG;
import com.PGApp.exception.PGIdNotFoundExcepton;
import com.PGApp.exception.PGNotFoundException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.sun.net.httpserver.Filter;

public class PGRepoImpl implements PGRepo {

	public List<PG> findAll(){
		MongoCollection<PG> collections= DbManager.getCollection();
	    List<PG> list=collections.find().into(new ArrayList<>());
		return list;
	    }

	
	
	public void addPg(PG pG) {
     	MongoCollection<PG> collection=DbManager.getCollection();
		collection.insertOne(pG);
	}
	public void addManyPg(List<PG> pG) {
		MongoCollection<PG> collection=DbManager.getCollection();
		collection.insertMany(pG);
	}

	@Override
	public void updatePg(String owner, double rent) throws PGNotFoundException{
		MongoCollection<PG> collection=DbManager.getCollection();
		List<PG> list=collection.find(Filters.eq("owner", owner)).into(new ArrayList<>());
		if(list.isEmpty())
			throw new PGNotFoundException("No PG found to update");
        collection.updateOne(Filters.eq("owner",owner),Updates.set("rent",rent));	
	}

	@Override
	public void deletePg(String owner) throws PGNotFoundException{
		MongoCollection<PG> collection=DbManager.getCollection();
		List<PG> list=collection.find(Filters.eq("owner",owner)).into(new ArrayList<>());
		if(list.isEmpty())
			 throw new PGNotFoundException("No pg is there for deletion");
        collection.deleteOne(Filters.eq("owner",owner));
	}

	@Override
	public List<PG> findByOwner(String owner) throws PGNotFoundException {
		MongoCollection<PG> collections= DbManager.getCollection();
	    List<PG> list=collections.find(Filters.eq("owner", owner)).into(new ArrayList<>());
	    if(list.isEmpty())
			throw new PGNotFoundException("Not Found Pgs with owner");
		return list;
	    
	}

	@Override
	public List<PG> findByCategory(String category) throws PGNotFoundException {
		MongoCollection<PG> collections= DbManager.getCollection();
		List<PG> list=collections.find(Filters.eq("category", category)).into(new ArrayList<>());
		if(list.isEmpty())
			throw new PGNotFoundException("Not Found Pgs with Category");
		return list;	
	    }

	@Override
	public PG findById(int PgId) throws PGIdNotFoundExcepton {
		MongoCollection<PG> collections= DbManager.getCollection();
	    Document doc=new Document("Id",PgId);
	    PG pG=collections.find(doc).first();
	    if(pG==null)
			throw new PGIdNotFoundExcepton("Not Found Pgs with this Id");
		return pG;	}

	@Override
	public List<PG> findByLesserRent(double rent) throws PGNotFoundException {
		MongoCollection<PG> collections= DbManager.getCollection();
	    List<PG> list=collections.find(Filters.lte("rent", rent)).into(new ArrayList<>());
	    if(list.isEmpty())
			throw new PGNotFoundException("Not Found Pgs with more lesser rent");
		return list;
	}

}
