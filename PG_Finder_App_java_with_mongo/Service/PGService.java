package com.PGApp.Service;
import com.PGApp.Model.PG;
import com.PGApp.exception.PGIdNotFoundExcepton;
import com.PGApp.exception.PGNotFoundException;

import java.util.*;
public interface PGService {
      void addPg(PG pG);
      void updatePg(String owner,double rent)throws PGNotFoundException;
      void deletePg(String owner)throws PGNotFoundException;
	
	
	   List<PG> getAll();
       List<PG> getByOwner(String owner) throws  PGNotFoundException;
       List<PG> getByCategory(String category) throws PGNotFoundException;
       PG getById(int PgId) throws PGIdNotFoundExcepton;
       List<PG> getByLesserRent(double rent) throws PGNotFoundException;
	    void addManyPg(List<PG> pG);

}
