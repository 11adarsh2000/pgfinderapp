package com.PGApp.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.BookApp.Model.Book;
import com.BookApp.exception.bookNotFoundException;
import com.PGApp.Model.PG;
import com.PGApp.exception.PGIdNotFoundExcepton;
import com.PGApp.exception.PGNotFoundException;
import com.PGApp.repository.DbManager;
import com.PGApp.repository.PGRepo;
import com.PGApp.repository.PGRepoImpl;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

public class PGServiceImpl implements PGService {
    PGRepo pgrepo=new PGRepoImpl();
	public List<PG> getAll() {
		return pgrepo.findAll();
	}

	public List<PG> getByOwner(String owner) throws PGNotFoundException {
		// TODO Auto-generated method stub
		List<PG> pG=pgrepo.findByOwner(owner);
		if(!pG.isEmpty())
			Collections.sort(pG,(b1,b2)->b1.getName().compareTo(b2.getName()));
		
		if(pG.isEmpty())
			throw new PGNotFoundException("pg not found");
			else return pG;		
	}

	@Override
	public List<PG> getByCategory(String category) throws PGNotFoundException {
		// TODO Auto-generated method stub
		List<PG> pG=pgrepo.findByCategory(category);
		if(!pG.isEmpty())
			Collections.sort(pG,(b1,b2)->b1.getName().compareTo(b2.getName()));
		if(pG.isEmpty())
			throw new PGNotFoundException("pg not found");
			else return pG;	}

	@Override
	public PG getById(int pgId) throws PGIdNotFoundExcepton {
		// TODO Auto-generated method stub
		PG pG=pgrepo.findById(pgId);
		if(pG!=null)
			return pG;
		else throw new PGIdNotFoundExcepton("Id Not Found");
	}

	@Override
	public List<PG> getByLesserRent(double rent) throws PGNotFoundException {
		// TODO Auto-generated method stub
		List<PG> pG=pgrepo.findByLesserRent(rent);
		if(!pG.isEmpty())
			Collections.sort(pG,(b1,b2)->b1.getName().compareTo(b2.getName()));
		if(pG.isEmpty())
		throw new PGNotFoundException("pg not found");
		else return pG;
	}

	
	public void addPg(PG pG) {
		// TODO Auto-generated method stub
		pgrepo.addPg(pG);
	}

	@Override
	public void updatePg(String owner, double rent) throws PGNotFoundException{
		// TODO Auto-generated method stub
		MongoCollection<PG> collection=DbManager.getCollection();
		List<PG> pG=collection.find(Filters.eq("owner", owner)).into(new ArrayList<>());
		if(pG==null)
			throw new PGNotFoundException("No PG found to update");
		pgrepo.updatePg(owner, rent);
	}

	@Override
	public void deletePg(String owner) throws PGNotFoundException{
		// TODO Auto-generated method stub
		MongoCollection<PG> collection=DbManager.getCollection();

		List<PG> list=collection.find(Filters.eq("owner",owner)).into(new ArrayList<>());
		if(list.isEmpty())
			 throw new PGNotFoundException("No PG is there to remove");		
		pgrepo.deletePg(owner);
    }
	public void addManyPg(List<PG> pG) {
		pgrepo.addManyPg(pG);
	}

	
	
	
}
