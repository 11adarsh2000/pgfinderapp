package com.mallapp.exception;

public class MallNotFoundException extends RuntimeException{
    public MallNotFoundException() {
    }

    public MallNotFoundException(String message) {
        super(message);
    }
}
