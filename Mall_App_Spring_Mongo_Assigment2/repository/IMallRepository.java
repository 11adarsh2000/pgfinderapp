package com.mallapp.repository;

import com.mallapp.model.Mall;
import com.mallapp.model.Mall;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMallRepository extends MongoRepository<Mall, Integer> {

    List<Mall> getByEntryFees(double entryFees);
    List<Mall> getByFamousForAndEntryFees(String FamousFor, double entryFees);
    List<Mall> getByFamousFor(String famousFor);
    List<Mall> getByType(String type);
    List<Mall> getByName(String MallName);
    List<Mall> getByNameAndFamousFor(String mallName, String famousFor);
    List<Mall> getByFamousForAndType(String famousFor, String type);
}
