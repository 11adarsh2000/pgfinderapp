package com.mallapp.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document
public class Mall {
    private String name;
    @Id
    private Integer mallId;
    private String famousFor;
    private String type;
    private double entryFees;


}
