package com.mallapp;

import com.mallapp.model.Mall;
import com.mallapp.service.IMallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

@SpringBootApplication
public class SpringMallappMongoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringMallappMongoApplication.class, args);
	}
		@Autowired
		private IMallService mallService;
		@Override
		public void run (String...args) throws Exception {
			Mall mall = new Mall("Central Mall", 1, "Biryani", "Food", 234.56);
			Mall mall1 = new Mall("Open Cap Mall", 2, "Kurtas", "Shopping", 562);
			Mall mall2 = new Mall("Samdariya Mall", 3, "South Indian Food", "Food", 112);
			Mall mall3= new Mall("Era Mall", 4, "Couch", "Food", 273.56);
			Mall mall4 = new Mall("Fashion Mall", 5, "Sarre", "Products", 98);
			Mall mall5 = new Mall("Lake Mall", 6, "Groceries", "Food", 345);
			Mall mall6= new Mall("Lucky Mall", 7, "Garments", "Food", 765);
			Mall mall7 = new Mall("Rove Mall", 8, "Ornament", "Shopping", 234);
			Mall mall8 = new Mall("Town Mall", 9, "Beauty", "Products", 654);

//		    mallService.addMall(mall);
//		    mallService.addMall(mall1);
//		    mallService.addMall(mall2);
//			mallService.addMall(mall3);
//     		mallService.addMall(mall4);
//	    	mallService.addMall(mall5);
//			mallService.addMall(mall6);
//		    mallService.addMall(mall7);
//		    mallService.addMall(mall8);
			//Mall malls = mallService.getById(12);
			List<Mall> malls=mallService.getByFamousForAndType("Couch","Food");
//			malls.stream().forEach(System.out::println);
			malls.stream().forEach(System.out::println);
	}



}
