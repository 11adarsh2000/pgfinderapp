package com.mallapp.service;

import com.mallapp.exception.MallNotFoundException;
import com.mallapp.model.Mall;
import com.mallapp.exception.MallNotFoundException;
import com.mallapp.model.Mall;

import java.util.List;

public interface IMallService {
    void addMall(Mall mall);
    void updateMall(Mall mall);
    void deleteMall(int mallId);

    Mall getById(int mallId) throws MallNotFoundException;
    List<Mall> getAllMall();
    List<Mall>  getByEntryFees(double entryFees) throws MallNotFoundException;
    List<Mall>  getByFamousFor(String famousFor)  throws MallNotFoundException;
    List<Mall>  getByType(String type) throws MallNotFoundException;
    List<Mall> getByName(String Name) throws MallNotFoundException;
    List<Mall> getByNameAndFamousFor(String Name,String famousFor) throws MallNotFoundException;
    List<Mall>  getByFamousForAndEntryFees(String famousFor,double fees) throws MallNotFoundException;
    List<Mall>  getByFamousForAndType(String famousFor,String type) throws MallNotFoundException;
}
