package com.mallapp.service;

import com.mallapp.exception.MallNotFoundException;
import com.mallapp.model.Mall;
import com.mallapp.model.Mall;
import com.mallapp.repository.IMallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IMallServiceImpl implements IMallService{
    private IMallRepository mallRepository;
    @Autowired
    public void setMallRepository(IMallRepository mallRepository) {
        this.mallRepository = mallRepository;
    }

    @Override
    public void addMall(Mall mall) {
          mallRepository.insert(mall);
    }

    @Override
    public void updateMall(Mall mall) {
        mallRepository.save(mall);
    }

    @Override
    public void deleteMall(int mallId) {
        mallRepository.deleteById(mallId);
    }

    @Override
    public Mall getById(int mallId) {
        return mallRepository.findById(mallId).orElseThrow(()->new MallNotFoundException("Invalid Id"));
    }

    @Override
    public List<Mall> getAllMall() {
        return mallRepository.findAll();
    }

    @Override
    public List<Mall> getByEntryFees(double entryEntryFees) {
        List<Mall> malls=mallRepository.getByEntryFees(entryEntryFees);
        if(malls.isEmpty())
            throw new MallNotFoundException("No Entry less then this");
        return malls;
    }

    @Override
    public List<Mall> getByFamousFor(String famousFor) {
        List<Mall> malls=mallRepository.getByFamousFor(famousFor);
        if(malls.isEmpty())
            throw new MallNotFoundException("No Famous Products Available");
        return malls;
    }

    @Override
    public List<Mall> getByType(String type) {
        List<Mall> malls=mallRepository.getByType(type);
        if(malls.isEmpty())
            throw new MallNotFoundException("No Mall of this type");
        return malls;
    }

    @Override
    public List<Mall> getByName(String mallName) {
        List<Mall> malls=mallRepository.getByName(mallName);
        if(malls.isEmpty())
            throw new MallNotFoundException("No mall of this name");
        return malls;
    }

    @Override
    public List<Mall> getByNameAndFamousFor(String mallName, String famousFor) {
        List<Mall> malls=mallRepository.getByNameAndFamousFor(mallName,famousFor);
        if(malls.isEmpty())
            throw new MallNotFoundException("No mall available");
        return malls;
    }

    @Override
    public List<Mall> getByFamousForAndEntryFees(String famousFor, double entryFees) {
        List<Mall> malls=mallRepository.getByFamousForAndEntryFees(famousFor,entryFees);
        if(malls.isEmpty())
            throw new MallNotFoundException("No mall available");
        return malls;
    }

    @Override
    public List<Mall> getByFamousForAndType(String famousFor, String type) {
        List<Mall> malls=mallRepository.getByFamousForAndType(famousFor,type);
        if(malls.isEmpty())
            throw new MallNotFoundException("No mall available");
        return malls;    }
}
